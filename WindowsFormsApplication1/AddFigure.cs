﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AreaCalc;

namespace WindowsFormsApplication1
{
    public partial class AddFigure : Form
    {
        public AddFigure()
        {
            InitializeComponent();
        }

        public List<IFigure> FigureList { get; set; }

        private FormFigure.FigureType _figureType;

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            try
            {
                switch (_figureType)
                {
                    case FormFigure.FigureType.Circle:
                        double radius = Convert.ToDouble(textBoxParam1.Text.Replace(".", ","));
                        AreaCalc.Сircle circle = new AreaCalc.Сircle(radius);
                        FigureList.Add(circle);
                        break;
                    case FormFigure.FigureType.Rectangle:
                        double sideRec1 = Convert.ToDouble(textBoxParam1.Text.Replace(".", ","));
                        double sideRec2 = Convert.ToDouble(textBoxParam2.Text.Replace(".", ","));
                        AreaCalc.Rectangle rectangle = new AreaCalc.Rectangle(sideRec1, sideRec2);
                        FigureList.Add(rectangle);
                        break;
                    case FormFigure.FigureType.Triangle:
                        double sideTr1 = Convert.ToDouble(textBoxParam1.Text.Replace(".", ","));
                        double sideTr2 = Convert.ToDouble(textBoxParam2.Text.Replace(".", ","));
                        double sideTr3 = Convert.ToDouble(textBoxParam3.Text.Replace(".", ","));
                        AreaCalc.Triangle triangle = new AreaCalc.Triangle(sideTr1, sideTr2, sideTr3);
                        FigureList.Add(triangle);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: " + ex.Message);
            }
            Close(); 
        }

        private void textBoxSide1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    if (e.KeyChar != '.')
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBoxSide2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    if (e.KeyChar != '.')
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBoxSide3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    if (e.KeyChar != '.')
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void figureChange_SelectedIndexChanged(object sender, EventArgs e)
        {
            _figureType = (FormFigure.FigureType)figureChange.SelectedIndex;
            MagicSwitch();
        }

        private void MagicSwitch()
        {
            switch (_figureType)
            {
                case FormFigure.FigureType.Circle:
                    label1.Text = "Radius";
                    label2.Visible = false;
                    textBoxParam2.Visible = false;
                    label3.Visible = false;
                    textBoxParam3.Visible = false;
                    break;
                case FormFigure.FigureType.Rectangle:
                    label1.Text = "Side 1";
                    label2.Visible = true;
                    textBoxParam2.Visible = true;
                    label3.Visible = false;
                    textBoxParam3.Visible = false;
                    break;
                case FormFigure.FigureType.Triangle:
                    label1.Text = "Side 1";
                    label2.Visible = true;
                    textBoxParam2.Visible = true;
                    label3.Visible = true;
                    textBoxParam3.Visible = true;
                    break;
            }
        }
    }
}
