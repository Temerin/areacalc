﻿namespace WindowsFormsApplication1
{
    partial class AddFigure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxParam2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxParam1 = new System.Windows.Forms.TextBox();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxParam3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.figureChange = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // textBoxParam2
            // 
            this.textBoxParam2.Location = new System.Drawing.Point(55, 69);
            this.textBoxParam2.Name = "textBoxParam2";
            this.textBoxParam2.Size = new System.Drawing.Size(83, 20);
            this.textBoxParam2.TabIndex = 13;
            this.textBoxParam2.Visible = false;
            this.textBoxParam2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSide2_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Side 2";
            this.label2.Visible = false;
            // 
            // textBoxParam1
            // 
            this.textBoxParam1.Location = new System.Drawing.Point(55, 43);
            this.textBoxParam1.Name = "textBoxParam1";
            this.textBoxParam1.Size = new System.Drawing.Size(83, 20);
            this.textBoxParam1.TabIndex = 11;
            this.textBoxParam1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSide1_KeyPress);
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(21, 131);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(107, 23);
            this.buttonCreate.TabIndex = 10;
            this.buttonCreate.Text = "Create";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Radius";
            // 
            // textBoxParam3
            // 
            this.textBoxParam3.Location = new System.Drawing.Point(55, 95);
            this.textBoxParam3.Name = "textBoxParam3";
            this.textBoxParam3.Size = new System.Drawing.Size(83, 20);
            this.textBoxParam3.TabIndex = 15;
            this.textBoxParam3.Visible = false;
            this.textBoxParam3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSide3_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Side 3";
            this.label3.Visible = false;
            // 
            // figureChange
            // 
            this.figureChange.FormattingEnabled = true;
            this.figureChange.Items.AddRange(new object[] {
            "Circle",
            "Rectangle",
            "Triangle"});
            this.figureChange.Location = new System.Drawing.Point(12, 12);
            this.figureChange.Name = "figureChange";
            this.figureChange.Size = new System.Drawing.Size(126, 21);
            this.figureChange.TabIndex = 16;
            this.figureChange.Text = "Circle";
            this.figureChange.SelectedIndexChanged += new System.EventHandler(this.figureChange_SelectedIndexChanged);
            // 
            // AddTriangle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(154, 161);
            this.Controls.Add(this.figureChange);
            this.Controls.Add(this.textBoxParam3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxParam2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxParam1);
            this.Controls.Add(this.buttonCreate);
            this.Controls.Add(this.label1);
            this.Name = "AddTriangle";
            this.Text = "AddTriangle";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxParam2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxParam1;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxParam3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox figureChange;
    }
}