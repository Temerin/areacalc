﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AreaCalc;
using System.Runtime.Serialization.Formatters.Binary;

namespace WindowsFormsApplication1
{
    public partial class FormFigure : Form
    {
        public List<IFigure> Figure = new List<IFigure>();

        public enum FigureType
        {
            Circle,
            Rectangle,
            Triangle
        };

        public FormFigure()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void buttonСircle_Click(object sender, EventArgs e)
        {
            AddCircle addForm = new AddCircle();
            addForm.FigureList = Figure;
            //addForm.Owner = this;
            addForm.ShowDialog();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void buttonRectangle_Click(object sender, EventArgs e)
        {
            AddRectangle addForm = new AddRectangle();
            addForm.FigureList = Figure;
            //addForm.Owner = this;
            addForm.ShowDialog();
        }

        private void FormFigure_Load(object sender, EventArgs e)
        {

        }

        private void FormFigure_Activated(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }
        private void UpdateDataGridView() 
        { 
            dataGridView.Rows.Clear(); 
            for (int i = 0; i < Figure.Count; i++) 
            { 
                DataGridViewRow row = new DataGridViewRow(); 
                row.CreateCells(dataGridView);
                if (Figure[i] is Сircle) { row.Cells[0].Value = "Круг"; }
                if (Figure[i] is AreaCalc.Rectangle) { row.Cells[0].Value = "Прямоугольник"; }
                if (Figure[i] is Triangle) { row.Cells[0].Value = "Треугольник"; }
                row.Cells[1].Value = Figure[i].Area; 
                dataGridView.Rows.Add(row); 
            } 
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView.SelectedRows)
            {
                Figure.RemoveAt(row.Index);
            }
            UpdateDataGridView();
        }

        private void buttonTriangle_Click(object sender, EventArgs e)
        {
            AddFigure addForm = new AddFigure();
            addForm.FigureList = Figure;
            //addForm.Owner = this;
            addForm.ShowDialog();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (Figure.Count != 0)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.DefaultExt = "acalc";
                saveFileDialog.Filter = "Калькулятор (*.acalc)|*.acalc";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        if (saveFileDialog.FileName != "")
                        {
                            FileStream stream = (FileStream) saveFileDialog.OpenFile();
                            BinaryFormatter binaryFormatter = new BinaryFormatter();
                            binaryFormatter.Serialize(stream, Figure);
                            stream.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ошибка: Не удалось сохранить файл на диск. Original error: " + ex.Message);
                    }
                }
            }
            else
            {
                MessageBox.Show("Ошибка: Список пуст.");
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "Калькулятор (*.acalc)|*.acalc";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream stream = null;
                    if ((stream = (FileStream)openFileDialog.OpenFile()) != null)
                    {
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        Figure = (List<IFigure>)binaryFormatter.Deserialize(stream);
                        stream.Close();
                        UpdateDataGridView();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка: Не удалось открыть файл с диска. Original error: " + ex.Message);
                }
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            FindFigure addForm = new FindFigure();
            addForm.FigureList = Figure;
            //addForm.Owner = this;
            addForm.ShowDialog();
        }
    }
}
